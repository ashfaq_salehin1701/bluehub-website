# Vuetify Alpha Theme
Thank you for purchasing the Theme and supporting Vuetify.

##### 📖 [Documentation](https://alpha-theme.vuetifyjs.com)
##### 🆘 [Support](https://discord.gg/w2qKXtD)

### Quick Start
Unzip the `alpha-theme-release.zip` file into any directory. Copy your theme of choice and from the cli:

```bash
cd path/to/project
yarn
yarn serve
```

### Alpha Feature Highlights
- Build with [vue-cli-3](https://cli.vuejs.org/)
- Integrated Packages
  - **[vue-router](https://router.vuejs.org/en/)** - _Application Routing_
  - **[vuex](https://vuex.vuejs.org/en/)** - _Application State_
  - **[vue-i18n](https://github.com/kazupon/vue-i18n)** - _Application Language_
  - **[vue-analytics](https://github.com/MatteoGabriele/vue-analytics)** - _Google Analytics_
  - **[axios]()** - Requests
  - Font Library
    - **[Material Design Icons](https://materialdesignicons.com/)**
- **31** high-quality open-source images
- **5** uniquely customized components
- **4** pre-made themes
  - [Company Site](https://alpha-construction.vuetifyjs.com)
  - [ECommerce Site](https://alpha-ecommerce.vuetifyjs.com)
  - [Portfolio Site](https://alpha-creative.vuetifyjs.com)
  - [SAAS Application](https://alpha-saas.vuetifyjs.com)
